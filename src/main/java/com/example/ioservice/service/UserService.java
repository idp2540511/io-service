package com.example.ioservice.service;

import com.example.ioservice.model.User;
import com.example.ioservice.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    public ResponseEntity<User> addUser(User user) {
        /* Check if the User already exists */
        Optional<User> userOptional = userRepository.findUserByEmail(user.getEmail());

        if (userOptional.isPresent()) {
            return new ResponseEntity<>(null, HttpStatus.CONFLICT);
        }

        User savedUser = userRepository.save(user);
        return new ResponseEntity<>(savedUser, HttpStatus.CREATED);
    }

    public ResponseEntity<User> updateUser(Integer userId, User user) {
        /* Check if the user exists */
        Optional<User> userOptional = userRepository.findById(userId);

        if (userOptional.isEmpty()) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        Optional<User> optionalUser = userRepository.findUserByEmail(user.getEmail());

        if (optionalUser.isPresent()) {
            return new ResponseEntity<>(null, HttpStatus.CONFLICT);
        }

        User userToBeUpdated = userOptional.get();
        userToBeUpdated.setEmail(user.getEmail());
        userToBeUpdated.setPassword(user.getPassword());
        userToBeUpdated.setFirstName(user.getFirstName());
        userToBeUpdated.setLastName(user.getLastName());

        User updateUser = userRepository.save(userToBeUpdated);
        return new ResponseEntity<>(updateUser, HttpStatus.OK);
    }

    public ResponseEntity<List<User>> getUsers() {
        return new ResponseEntity<>(userRepository.findAll(), HttpStatus.OK);
    }

    public ResponseEntity<User> findUserByEmail(String username) {
        Optional<User> optionalUser = userRepository.findUserByEmail(username);

        if (optionalUser.isEmpty()) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(optionalUser.get(), HttpStatus.OK);
    }

    public ResponseEntity<String> deleteUser(Integer userId) {
        /* Check if the user exists */
        Optional<User> userOptional = userRepository.findById(userId);

        if (userOptional.isEmpty()) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        User user = userOptional.get();
        userRepository.delete(user);
        return new ResponseEntity<>("User with id " + userId
                + " has been deleted successfully", HttpStatus.OK);
    }
}
