package com.example.ioservice.service;

import com.example.ioservice.model.Book;
import com.example.ioservice.repository.BookRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class BookService {
    private final BookRepository bookRepository;

    public ResponseEntity<Book> addBook(Book book) {
        /* Check if the book already exists */
        Optional<Book> bookOptional = bookRepository.findBookByAuthorAndName(book.getAuthor(), book.getName());

        if(bookOptional.isPresent()) {
            return new ResponseEntity<>(null, HttpStatus.CONFLICT);
        }

        Book savedBook = bookRepository.save(book);
        return new ResponseEntity<>(savedBook, HttpStatus.CREATED);
    }

    public ResponseEntity<Book> updateBook(Integer bookId, Book book) {
        /* Check if the event exists */
        Optional<Book> bookOptional = bookRepository.findById(bookId);

        if(bookOptional.isEmpty()) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        Book bookToBeUpdated = bookOptional.get();
        bookToBeUpdated.setAuthor(book.getAuthor());
        bookToBeUpdated.setName(book.getName());
        bookToBeUpdated.setPrice(book.getPrice());
        bookToBeUpdated.setGenre(book.getGenre());
        bookToBeUpdated.setPublicationYear(book.getPublicationYear());

        Book updateBook = bookRepository.save(bookToBeUpdated);
        return new ResponseEntity<>(updateBook, HttpStatus.OK);
    }

    public ResponseEntity<List<Book>> getBooks() {
        return new ResponseEntity<>(bookRepository.findAll(), HttpStatus.OK);
    }

    public ResponseEntity<String> deleteBook(Integer bookId) {
        /* Check if the book exists */
        Optional<Book> bookOptional = bookRepository.findById(bookId);

        if(bookOptional.isEmpty()) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        Book book = bookOptional.get();
        bookRepository.delete(book);
        return new ResponseEntity<>("Book with id " + bookId
                + " has been deleted successfully", HttpStatus.OK);
    }
}
