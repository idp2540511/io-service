package com.example.ioservice.service;


import com.example.ioservice.model.Event;
import com.example.ioservice.repository.EventRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class EventService {

    private EventRepository eventRepository;

    public ResponseEntity<Event> addEvent(Event event) {
        /* Check if the event already exists */
        Optional<Event> eventOptional = eventRepository.findByName(event.getName());
        if (eventOptional.isPresent()) {
            return new ResponseEntity<>(null, HttpStatus.CONFLICT);
        }

        Event savedEvent =  eventRepository.save(event);
        return new ResponseEntity<>(savedEvent, HttpStatus.CREATED);
    }

    public ResponseEntity<Event> updateEvent(Integer eventId, Event event) {
        /* Check if the event already exists */
        Optional<Event> eventOptional = eventRepository.findById(eventId);
        if (eventOptional.isEmpty()) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        Event eventToBeUpdated = eventOptional.get();
        eventToBeUpdated.setType(event.getType());
        eventToBeUpdated.setName(event.getName());
        eventToBeUpdated.setAddress(event.getAddress());
        eventToBeUpdated.setTicketPrice(event.getTicketPrice());

        Event updatedEvent = eventRepository.save(eventToBeUpdated);
        return new ResponseEntity<>(updatedEvent, HttpStatus.OK);
    }

    public ResponseEntity<String> deleteEvent(Integer eventId) {
        /* Check if the event that will be deleted exists */
        Optional<Event> eventOptional = eventRepository.findById(eventId);
        if (eventOptional.isEmpty()) {
            return new ResponseEntity<>("", HttpStatus.NOT_FOUND);
        }

        Event event = eventOptional.get();
        eventRepository.delete(event);

        return new ResponseEntity<>("Event with id " + eventId
                + " has been deleted successfully.", HttpStatus.OK);
    }

    public List<Event> getAllEvents() {
        return eventRepository.findAll();
    }

}
