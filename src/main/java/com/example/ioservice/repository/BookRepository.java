package com.example.ioservice.repository;

import com.example.ioservice.model.Book;
import jakarta.validation.constraints.NotBlank;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BookRepository extends JpaRepository<Book, Integer> {
    Optional<Book> findBookByAuthorAndName(@NotBlank String author, @NotBlank String name);
}
