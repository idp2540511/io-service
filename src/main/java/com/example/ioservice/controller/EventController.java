package com.example.ioservice.controller;

import com.example.ioservice.model.Event;
import com.example.ioservice.service.EventService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("api/events")
public class EventController {

    private EventService eventService;

    @PostMapping
    public ResponseEntity<Event> addEvent(@RequestBody @Valid Event event) {
        return eventService.addEvent(event);
    }

    @PutMapping("{eventId}")
    public ResponseEntity<Event> updateEvent(@PathVariable Integer eventId,
                                             @RequestBody @Valid Event event) {
        return eventService.updateEvent(eventId, event);
    }

    @DeleteMapping("{eventId}")
    public ResponseEntity<String> deleteEvent(@PathVariable Integer eventId) {
        return eventService.deleteEvent(eventId);
    }

    @GetMapping
    public List<Event> getAllEvents() {
        return eventService.getAllEvents();
    }

}
