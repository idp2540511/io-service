package com.example.ioservice.controller;

import com.example.ioservice.model.User;
import com.example.ioservice.service.UserService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@AllArgsConstructor
@RequestMapping("api/users")
public class UserController {
    private final UserService userService;

    @PostMapping("/add")
    public ResponseEntity<User> addUser(@RequestBody @Valid User user) {
        return userService.addUser(user);
    }

    @GetMapping("/find-user/{email}")
    public ResponseEntity<User> findUser(@PathVariable String email) {
        return userService.findUserByEmail(email);
    }


    @PutMapping("{userId}")
    public ResponseEntity<User> updateUser(@PathVariable Integer userId,
                                           @RequestBody @Valid User user) {
        return userService.updateUser(userId, user);
    }

    @GetMapping
    public ResponseEntity<List<User>> getUsers() {
        return userService.getUsers();
    }

    @DeleteMapping("{userId}")
    public ResponseEntity<String> deleteUser(@PathVariable Integer userId) {
        return userService.deleteUser(userId);
    }
}
