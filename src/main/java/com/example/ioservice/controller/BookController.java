package com.example.ioservice.controller;

import com.example.ioservice.model.Book;
import com.example.ioservice.service.BookService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@AllArgsConstructor
@RequestMapping("api/books")
public class BookController {
    private final BookService bookService;

    @PostMapping
    public ResponseEntity<Book> addBook(@RequestBody @Valid Book book) {
        return bookService.addBook(book);
    }

    @PutMapping("{bookId}")
    public ResponseEntity<Book> updateBook(@PathVariable Integer bookId,
                                           @RequestBody @Valid Book book) {
        return bookService.updateBook(bookId, book);
    }

    @GetMapping
    public ResponseEntity<List<Book>> getBooks() {
        return bookService.getBooks();
    }

    @DeleteMapping("{bookId}")
    public ResponseEntity<String> deleteBook(@PathVariable Integer bookId) {
        return bookService.deleteBook(bookId);
    }
}
