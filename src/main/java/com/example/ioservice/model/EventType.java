package com.example.ioservice.model;

public enum EventType {
    BOOK_LAUNCH,
    PARTY,
    BALL
}
