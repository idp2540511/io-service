package com.example.ioservice.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import lombok.*;

@Entity
@Table(name = "_Users")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "email", nullable = false, unique = true)
    @NotBlank
    private String email;

    @Column(name = "password", nullable = false)
    @NotBlank
    private String password;

    @Column(name = "firstName", nullable = false)
    @NotBlank
    private String firstName;

    @Column(name = "lastName", nullable = false)
    @NotBlank
    private String lastName;

    @Enumerated(value = EnumType.STRING)
    private Role role;
}
