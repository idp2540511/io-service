package com.example.ioservice.model;


import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

import static jakarta.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "Events")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Event implements Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private int id;

    @Column(name = "name", nullable = false, unique = true)
    @NotBlank
    private String name;

    @Column(name = "type", nullable = false)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private EventType type;

    @Column(name = "address", nullable = false)
    @NotBlank
    private String address;

    @Column(name = "price", nullable = false)
    @NotNull
    private float ticketPrice;
}
