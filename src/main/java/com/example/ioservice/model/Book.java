package com.example.ioservice.model;


import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "Books")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name", nullable = false)
    @NotBlank
    private String name;

    @Column(name = "author", nullable = false)
    @NotBlank
    private String author;

    @Column(name = "genre", nullable = false)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Genres genre;

    @Column(name = "publication_year", nullable = false)
    @NotNull
    private Integer publicationYear;

    @Column(name = "price", nullable = false)
    @NotNull
    private Float price;
}
