package com.example.ioservice.model;

public enum Genres {
    FICTION,
    MYSTERY,
    THRILLER,
    ROMANCE,
    FANTASY,
    HORROR

}
