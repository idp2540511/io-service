package com.example.ioservice.model;

public enum Role {
    ROLE_ADMIN,
    ROLE_USER
}
