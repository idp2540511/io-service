FROM openjdk:17-alpine
WORKDIR /app
COPY target/io-service-0.0.1-SNAPSHOT.jar /app/io-service.jar
EXPOSE 8080